(function($) {

    "use strict";



   /*------------------------------------------------------------
    *  search
    * ------------------------------------------------------ */
    $(function () {
    $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });
    
    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });
    
    
    //Do not include! This prevents the form from submitting for DEMO purposes only!
    $('form').submit(function(event) {
        event.preventDefault();
        return false;
    })
});
	
	
	 /*------------------------------------------------------------
    *               Banner-Slider
    * ------------------------------------------------------ */
	
	
	
	$('.banner-slider').owlCarousel({
 		loop: true,
 		margin: 0,
		 dots:false,
		autoplay:true,
autoplayTimeout:3000,
autoplayHoverPause:true,
		responsive: {
 			0: {
 				items: 1
 			},
			360:{
				items: 1
			},
 			600: {
 				items:2
 			},
 			1000: { 
 				items: 4
 			}	
 		}
		
 	});
	
	
	
	
	
	$('.top-blog-slider-two').owlCarousel({
 		loop: true,
 		margin: 20,
				 navText: ["<i class='fas fa-arrow-left'></i>", "<i class=' fas fa-arrow-right' ></i>"],
		 dots:false,
		nav:true,
		autoplay:true,
autoplayTimeout:3000,
autoplayHoverPause:true,
		responsive: {
 			0: {
 				items: 1
 			},
			360:{
				items: 1
			},
 			600: {
 				items:2
 			},
 			1000: { 
 				items: 3
 			}	
 		}
 	});
	
		 /*------------------------------------------------------------
    *           sidebar-Slider
    * ------------------------------------------------------ */
	
	
	
	$('.widget-slider-item').owlCarousel({
 		loop: true,
 		margin: 0,
				 navText: ["<i class='fas fa-arrow-left'></i>", "<i class=' fas fa-arrow-right' ></i>"],
		 dots:false,
		nav:true,
		autoplay:true,
autoplayTimeout:3000,
autoplayHoverPause:true,
		responsive: {
 			0: {
 				items: 1
 			},
			360:{
				items: 1
			},
 			600: {
 				items:2
 			},
 			1000: { 
 				items: 1
 			}	
 		}
 	});
	
	
		
		 /*------------------------------------------------------------
    *          main-content-Slider
    * ------------------------------------------------------ */
	
	
	
	$('.blog-slider-home').owlCarousel({
 		loop: true,
 		margin: 0,
				 navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
		 dots:false,
		nav:true,
		autoplay:true,
autoplayTimeout:3000,
autoplayHoverPause:true,
		responsive: {
 			0: {
 				items: 1
 			},
			360:{
				items: 1
			},
 			600: {
 				items:2
 			},
 			1000: { 
 				items: 1
 			}	
 		}
 	});


// --------------------SURAIYA JS CODE START-----------------
	// Init MixitUp
	$('#filtering-container').mixItUp({
		selectors: {
			filter: '.filter-one'
		}
	});

// --------------------SURAIYA JS CODE END-----------------
	
})(jQuery);